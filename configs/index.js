module.exports = {
  passwordSaltRound: 10,
  jwtTokenSignature: 'Zm9yLXRva2VuLXF1ZXVlLW1hbmFnZW1lbnQtcGVjay1hcm0tbmV3',
  jwtRefreshTokenSignature: 'Zm9yLXJlZnJlc2gtdG9rZW4tcXVldWUtbWFuYWdlbWVudC1wZWNrLWFybS1uZXc=',
  tokenLife: "1m",
  refreshTokenLife: "2d"
};
