const jwt = require("jsonwebtoken");
const config = require("../configs");

const AuthMiddleware = {
  verifyToken(req, res, next) {
    try {
      const token = req.headers["authorization"] || "";
      const decoded = jwt.verify(token, config.jwtTokenSignature);
      if (!decoded?._id) throw new Error("No _id for user");

      req.headers["userId"] = decoded._id;
      req.headers["permission"] = decoded.permission;
      return next();
    } catch (error) {
      if (error?.message.includes("expired")) {
        if (req.originalUrl === "/auth/token") return next();
      }
      return res.status(401).json({
        message: "Unauthorized.",
      });
    }
  },

  verifyAdmin(req, res, next) {
    try {
      if (req.headers["permission"] !== "admin")
        throw new Error("Permission denied");
      return next();
    } catch (error) {
      return res.status(401).json({
        message: "Unauthorized.",
      });
    }
  },

  verifySuperAdmin(req, res, next) {
    try {
      if (req.headers["permission"] !== "super_admin")
        throw new Error("Permission denied");
      return next();
    } catch (error) {
      console.log(error?.message);
      return res.status(401).json({
        message: "Unauthorized.",
      });
    }
  },
};

module.exports = AuthMiddleware;
