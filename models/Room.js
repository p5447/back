const mongoose = require("mongoose");
const softDelete = require("mongoosejs-soft-delete");
const Schema = mongoose.Schema;

const RoomSchema = new Schema(
  {
    company: {
      type: Schema.Types.ObjectId,
      ref: "Company",
    },
    number: { type: Number, required: true, default: 1 },
    currentQueue: {
      type: Schema.Types.ObjectId,
      ref: "Queue",
    },
    queueTypes: [
      {
        type: Schema.Types.ObjectId,
        ref: "QueueType",
      },
    ],
    users: [
      {
        type: Schema.Types.ObjectId,
        ref: "User",
      },
    ],
  },
  {
    timestamps: true,
  }
);

RoomSchema.plugin(softDelete);
module.exports = mongoose.model("Room", RoomSchema, "Room");
