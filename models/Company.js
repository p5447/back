const mongoose = require("mongoose");
const softDelete = require("mongoosejs-soft-delete");
const Schema = mongoose.Schema;

const CompanySchema = new Schema(
  {
    name: { type: String },
    specify: { type: String },
  },
  {
    timestamps: true,
  }
);

CompanySchema.plugin(softDelete);
module.exports = mongoose.model("Company", CompanySchema, "Company");
