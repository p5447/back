const mongoose = require("mongoose");
const softDelete = require("mongoosejs-soft-delete");
const Schema = mongoose.Schema;

const SettingSchema = new Schema(
  {
    openQueue: { type: Boolean, default: false, required: true },
    company: {
      type: Schema.Types.ObjectId,
      ref: "Company",
    },
  },
  {
    timestamps: true,
  }
);

SettingSchema.plugin(softDelete);
module.exports = mongoose.model("Setting", SettingSchema, "Setting");
