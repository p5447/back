const mongoose = require("mongoose");
const softDelete = require("mongoosejs-soft-delete");
const Schema = mongoose.Schema;

const QueueTypeSchema = new Schema(
  {
    title: { type: String, required: true },
    code: { type: String, required: true },
    description: { type: String, default: "" },
    company: {
      type: Schema.Types.ObjectId,
      ref: "Company",
    },
  },
  {
    timestamps: true,
  }
);

QueueTypeSchema.plugin(softDelete);
module.exports = mongoose.model("QueueType", QueueTypeSchema, "QueueType");
