const mongoose = require("mongoose");
const softDelete = require("mongoosejs-soft-delete");
const Schema = mongoose.Schema;

const QueueSchema = new Schema(
  {
    queueType: {
      type: Schema.Types.ObjectId,
      ref: "QueueType",
    },
    company: {
      type: Schema.Types.ObjectId,
      ref: "Company",
    },
    successBy: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    destroyBy: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    no: { type: Number, required: true },
    noForQueueType: { type: Number, required: true },
    name: { type: String },
    tel: { type: String },
    waiting: { type: Boolean, required: true, default: true },
  },
  {
    timestamps: true,
  }
);

QueueSchema.plugin(softDelete);
module.exports = mongoose.model("Queue", QueueSchema, "Queue");
