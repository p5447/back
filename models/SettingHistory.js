const mongoose = require("mongoose");
const softDelete = require("mongoosejs-soft-delete");
const Schema = mongoose.Schema;

const SettingHistorySchema = new Schema(
  {
    openQueue: { type: Boolean, required: true },
    company: {
      type: Schema.Types.ObjectId,
      ref: "Company",
    },
  },
  {
    timestamps: true,
  }
);

SettingHistorySchema.plugin(softDelete);
module.exports = mongoose.model(
  "SettingHistory",
  SettingHistorySchema,
  "SettingHistory"
);
