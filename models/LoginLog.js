const mongoose = require("mongoose");
const softDelete = require("mongoosejs-soft-delete");
const Schema = mongoose.Schema;

const LoginLogSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
  },
  {
    timestamps: true,
  }
);

LoginLogSchema.plugin(softDelete);
module.exports = mongoose.model("LoginLog", LoginLogSchema, "LoginLog");
