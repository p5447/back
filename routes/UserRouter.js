const express = require("express");
const UserController = require("../controllers/UserController");
const AuthMiddleware = require("../middlewares/AuthMiddleware");
const User = require("../models/User");

const UserRouter = express.Router();

UserRouter.get("/", [AuthMiddleware.verifyToken], UserController.getUsers);
UserRouter.get(
  "/user/:_id",
  [AuthMiddleware.verifyToken],
  UserController.getUserById
);
UserRouter.get(
  "/:companyId",
  [AuthMiddleware.verifyToken],
  UserController.getUsers
);
UserRouter.post("/", [AuthMiddleware.verifyToken], UserController.createUser);
UserRouter.put("/", [AuthMiddleware.verifyToken], UserController.editUser);
UserRouter.put(
  "/reset-password",
  [AuthMiddleware.verifyToken],
  UserController.resetPassword
);
UserRouter.delete("/", [AuthMiddleware.verifyToken], UserController.deleteUser);

module.exports = UserRouter;
