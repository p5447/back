const express = require("express");
const CompanyController = require("../controllers/CompanyController");
const AuthMiddleware = require("../middlewares/AuthMiddleware");

const CompanyRouter = express.Router();

CompanyRouter.get("/", CompanyController.getCompanies);
CompanyRouter.get("/:_id", CompanyController.getCompanyById);
CompanyRouter.post(
  "/",
  [AuthMiddleware.verifyToken, AuthMiddleware.verifySuperAdmin],
  CompanyController.createCompany
);
CompanyRouter.delete(
  "/",
  [AuthMiddleware.verifyToken, AuthMiddleware.verifySuperAdmin],
  CompanyController.deleteCompany
);

module.exports = CompanyRouter;
