const express = require("express");
const { getQueueById } = require("../controllers/QueueController");
const QueueController = require("../controllers/QueueController");
const AuthMiddleware = require("../middlewares/AuthMiddleware");

const QueueRouter = express.Router();

QueueRouter.get("/forward-queue/:roomId", QueueController.getForwardQueue);
QueueRouter.get("/:queueId", QueueController.getQueueById);
QueueRouter.post("/", QueueController.createQueue);
QueueRouter.post(
  "/next-queue",
  [AuthMiddleware.verifyToken],
  QueueController.nextQueue
);

module.exports = QueueRouter;
