const express = require("express");
const DashboardController = require("../controllers/DashboardController");
const AuthMiddleware = require("../middlewares/AuthMiddleware");

const DashboardRouter = express.Router();

DashboardRouter.post(
  "/",
  [AuthMiddleware.verifyToken],
  DashboardController.getDashboard
);
DashboardRouter.post(
  "/admin",
  [AuthMiddleware.verifyToken, AuthMiddleware.verifySuperAdmin],
  DashboardController.getDashboardAdmin
);

module.exports = DashboardRouter;
