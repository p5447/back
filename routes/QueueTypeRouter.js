const express = require("express");
const QueueTypeController = require("../controllers/QueueTypeController");
const AuthMiddleware = require("../middlewares/AuthMiddleware");

const QueueTypeRouter = express.Router();

QueueTypeRouter.get(
  "/",
  QueueTypeController.getQueueTypes
);
QueueTypeRouter.post(
  "/",
  [AuthMiddleware.verifyToken],
  QueueTypeController.createQueueType
);
QueueTypeRouter.put(
  "/",
  [AuthMiddleware.verifyToken],
  QueueTypeController.editQueueType
);
QueueTypeRouter.delete(
  "/",
  [AuthMiddleware.verifyToken],
  QueueTypeController.deleteQueueType
);

module.exports = QueueTypeRouter;
