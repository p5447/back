const express = require("express");
const SettingController = require("../controllers/SettingController");
const AuthMiddleware = require("../middlewares/AuthMiddleware");

const SettingRouter = express.Router();

SettingRouter.get("/queue", SettingController.isOpenQueue);
SettingRouter.post(
  "/open-queue",
  [AuthMiddleware.verifyToken],
  SettingController.openQueue
);
SettingRouter.post(
  "/close-queue",
  [AuthMiddleware.verifyToken],
  SettingController.closeQueue
);

module.exports = SettingRouter;
