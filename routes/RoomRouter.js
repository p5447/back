const express = require("express");
const RoomController = require("../controllers/RoomController");
const AuthMiddleware = require("../middlewares/AuthMiddleware");

const RoomRouter = express.Router();

RoomRouter.get("/", RoomController.getRooms);
RoomRouter.get(
  "/by-user",
  [AuthMiddleware.verifyToken],
  RoomController.getRoomsByUser
);
RoomRouter.get(
  "/:id",
  [AuthMiddleware.verifyToken],
  RoomController.getRoomById
);
RoomRouter.post("/", [AuthMiddleware.verifyToken], RoomController.createRoom);
RoomRouter.delete("/", [AuthMiddleware.verifyToken], RoomController.deleteRoom);

module.exports = RoomRouter;
