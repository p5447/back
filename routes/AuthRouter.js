const express = require("express");
const AuthController = require("../controllers/AuthController");
const AuthMiddleware = require("../middlewares/AuthMiddleware");

const AuthRouter = express.Router();

AuthRouter.post("/login", AuthController.login);
AuthRouter.post(
  "/token",
  [AuthMiddleware.verifyToken],
  AuthController.newToken
);

module.exports = AuthRouter;
