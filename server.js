const express = require("express");
const app = express();

const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");

require("dotenv").config();
const mongoose = require("mongoose");
const UserController = require("./controllers/UserController");

mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
  console.log("connect database successful");
  UserController.initSuperAdmin();
});

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(cors());

const QueueTypeRouter = require("./routes/QueueTypeRouter");
const RoomRouter = require("./routes/RoomRouter");
const QueueRouter = require("./routes/QueueRouter");
const SettingRouter = require("./routes/SettingRoute");
const AuthRouter = require("./routes/AuthRouter");
const CompanyRouter = require("./routes/CompanyRouter");
const UserRouter = require("./routes/UserRouter");
const DashboardRouter = require("./routes/DashboardRouter");

app.use("/auth", AuthRouter);
app.use("/queue-type", QueueTypeRouter);
app.use("/room", RoomRouter);
app.use("/queue", QueueRouter);
app.use("/setting", SettingRouter);
app.use("/company", CompanyRouter);
app.use("/user", UserRouter);
app.use("/dashboard", DashboardRouter);

const server = require("http").Server(app);
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});
const port = process.env.PORT || 8100;

io.on("connection", (socket) => {
  /* SOCKET - CORE EVENTS  */
  console.log("a user connected ", socket.id);
  socket.on("connect", (message) => {
    console.log("connected: " + message + "socket_id:" + socket.id);
  });

  socket.on("disconnect", (data) => {
    console.log("user disconnected:" + socket.id);
  });

  socket.on("error", (err) => {
    console.log("received error from client:", socket.id, " Error :", err);
  });

  socket.on("next_queue", (data) => {
    io.emit("next_queue", data);
  });

  socket.on("create_queue", (data) => {
    io.emit("create_queue", "");
  });
});

server.listen(port, () => {
  console.log("created server");
});
