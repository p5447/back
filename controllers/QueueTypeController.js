const { ObjectId } = require("mongodb");
const QueueType = require("../models/QueueType");
const Room = require("../models/Room");

const QueueTypeController = {
  async getQueueTypes(req, res, next) {
    try {
      const companyId = req.headers["company"] || null;
      const queueTypes = await QueueType.find({
        company: ObjectId(companyId),
      }).sort({ _id: 1 });
      return res.status(200).json({
        success: true,
        message: "get queue-types success",
        data: queueTypes || [],
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async createQueueType(req, res, next) {
    try {
      const companyId = req.headers["company"] || null;
      const existQueueType = await QueueType.findOne({
        code: req.body.code,
      });

      if (existQueueType) {
        return res.status(200).json({
          message: "Code is duplicate.",
        });
      }

      const queueType = new QueueType({
        title: req.body.title,
        code: req.body.code,
        description: req.body.description,
        company: ObjectId(companyId),
      });

      await queueType.save();
      return res.status(200).json({
        success: true,
        message: "Create new queue type success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async editQueueType(req, res, next) {
    try {
      const existQueueType = await QueueType.findOne({
        _id: req.body._id,
      });

      if (!existQueueType || existQueueType.deleted) {
        return res.status(200).json({
          message: "Queue type not found.",
        });
      }

      const existCode = await QueueType.findOne({
        code: req.body.code,
      });

      if (existCode && existCode._id.toString() !== req.body._id) {
        return res.status(200).json({
          message: "Code is duplicate.",
        });
      }

      await QueueType.updateOne(
        { _id: req.body._id },
        {
          $set: {
            title: req.body.title,
            code: req.body.code,
            description: req.body.description,
          },
        }
      );

      return res.status(200).json({
        success: true,
        message: "Edit queue type success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async deleteQueueType(req, res, next) {
    try {
      const existQueueType = await QueueType.findOne({
        _id: ObjectId(req.body._id),
      });

      if (!existQueueType || existQueueType.deleted) {
        return res.status(200).json({
          message: "Queue type not found.",
        });
      }

      await QueueType.removeOne({
        _id: ObjectId(req.body._id),
      });

      return res.status(200).json({
        success: true,
        message: "Delete queue type success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
};

module.exports = QueueTypeController;
