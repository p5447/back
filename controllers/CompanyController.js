const { ObjectId } = require("mongodb");
const Company = require("../models/Company");
const Queue = require("../models/Queue");
const Room = require("../models/Room");
const Setting = require("../models/Setting");
const QueueType = require("../models/QueueType");
const User = require("../models/User");
const { initSetting } = require("./SettingController");

const CompanyController = {
  async getCompanies(req, res, next) {
    try {
      const companies = await Company.find({ deleted: false }).sort({
        _id: -1,
      });
      return res.status(200).json({
        success: true,
        message: "Get companies success",
        data: companies,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async getCompanyById(req, res, next) {
    try {
      const company = await Company.findOne({ _id: ObjectId(req.params._id) });
      if (!company) {
        return res.status(200).json({
          message: "Company not found.",
        });
      }

      return res.status(200).json({
        success: true,
        message: "Get company success",
        data: company,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async createCompany(req, res, next) {
    try {
      let { name, specify } = req.body;
      specify = specify.toLowerCase().trim();
      const existSpecifyCompany = await Company.findOne({
        specify: specify,
      });

      if (existSpecifyCompany) {
        return res.status(200).json({
          message: "Specify is exist.",
        });
      }

      const newCompany = new Company({
        name: name,
        specify: specify,
      });

      await newCompany.save();

      const company = await Company.findOne({ specify: specify });
      const responseInitSetting = await initSetting(company._id);
      if (!responseInitSetting) {
        await Company.deleteOne({ specify: specify });
        throw new Error();
      }
      return res.status(200).json({
        success: true,
        message: "Create company success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async deleteCompany(req, res, next) {
    try {
      const existCompany = await Company.findOne({
        _id: ObjectId(req.body._id),
      });
      if (!existCompany) {
        return res.status(200).json({
          message: "Company not found.",
        });
      }

      await Company.deleteOne({ _id: ObjectId(existCompany._id) });
      await Queue.deleteMany({ company: ObjectId(existCompany._id) });
      await Room.deleteMany({ company: ObjectId(existCompany._id) });
      await Setting.deleteMany({ company: ObjectId(existCompany._id) });
      await QueueType.deleteMany({ company: ObjectId(existCompany._id) });
      await User.deleteMany({ company: ObjectId(existCompany._id) });

      return res.status(200).json({
        success: true,
        message: "Delete company success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
};

module.exports = CompanyController;
