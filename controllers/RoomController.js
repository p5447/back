const { ObjectId } = require("mongodb");
const Room = require("../models/Room");
const Setting = require("../models/Setting");
const QueueType = require("../models/QueueType");
const User = require("../models/User");

const RoomController = {
  async getRooms(req, res, next) {
    try {
      const companyId = req.headers["company"] || null;
      const rooms = await Room.find({
        company: ObjectId(companyId),
        deleted: false,
      })
        .populate({
          path: "currentQueue",
          populate: {
            path: "queueType",
          },
        })
        .populate("queueTypes")
        .sort({ _id: 1 });
      return res.status(200).json({
        success: true,
        message: "get rooms success",
        data: rooms,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async getRoomsByUser(req, res, next) {
    try {
      if (req.headers["permission"] === "admin")
        return RoomController.getRooms(req, res, next);

      const companyId = req.headers["company"] || null;
      const rooms = await Room.find({
        company: ObjectId(companyId),
        users: [ObjectId(req.headers["userId"])],
        deleted: false,
      })
        .populate({
          path: "currentQueue",
          populate: {
            path: "queueType",
          },
        })
        .populate("queueTypes")
        .sort({ _id: 1 });
      return res.status(200).json({
        success: true,
        message: "get rooms success",
        data: rooms,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async getRoomById(req, res, next) {
    try {
      const room = await Room.findOne({
        _id: ObjectId(req.params.id),
      }).populate({
        path: "currentQueue",
        populate: {
          path: "queueType",
        },
      });
      if (!room) {
        return res.status(200).json({
          message: "Room not found",
        });
      }

      return res.status(200).json({
        success: true,
        message: "get room success",
        data: room,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async createRoom(req, res, next) {
    try {
      const companyId = req.headers["company"] || null;

      const setting = await Setting.findOne({
        company: ObjectId(companyId),
      });
      if (setting.openQueue) {
        return res.status(200).json({
          message: "Must have close queue in setting before create room",
        });
      }

      const { queueTypes, users } = req.body;

      if (!queueTypes) {
        return res.status(200).json({
          message: "Some queue type is invalid.",
        });
      }

      const queueTypeRefIds = queueTypes
        .split(",")
        .map((value) => ObjectId(value));

      const userRefIds = users.split(",").map((value) => ObjectId(value));

      const queueTypeQueries = await QueueType.find({
        company: ObjectId(companyId),
        _id: { $in: queueTypeRefIds },
      });

      if (
        !queueTypeQueries ||
        queueTypeQueries.length !== queueTypeRefIds.length
      ) {
        return res.status(200).json({
          message: "Some queue type is invalid.",
        });
      }

      const userQueries = await User.find({
        company: ObjectId(companyId),
        _id: { $in: userRefIds },
      });

      if (!userQueries || userQueries.length !== userRefIds.length) {
        return res.status(200).json({
          message: "Some user is invalid.",
        });
      }

      const lastRoom = await Room.findOne({
        company: ObjectId(companyId),
        deleted: false,
      }).sort({ number: -1 });

      if (!lastRoom) {
        const room = new Room({
          company: ObjectId(companyId),
          queueTypes: queueTypeRefIds,
          users: userRefIds,
        });
        await room.save();
      } else {
        const existRooms = await Room.find({
          company: ObjectId(companyId),
          deleted: false,
        }).sort({
          number: 1,
        });

        if (existRooms.length === lastRoom.number) {
          const room = new Room({
            number: lastRoom.number + 1,
            company: ObjectId(companyId),
            queueTypes: queueTypeRefIds,
            users: userRefIds,
          });
          await room.save();
        } else {
          for (let [index, room] of existRooms.entries()) {
            await Room.updateOne(
              {
                _id: room._id,
              },
              {
                $set: {
                  number: index + 1,
                },
              }
            );

            if (index + 1 === existRooms.length) {
              const room = new Room({
                number: index + 2,
                company: ObjectId(companyId),
                queueTypes: queueTypeRefIds,
                users: userRefIds,
              });
              await room.save();
            }
          }
        }
      }

      return res.status(200).json({
        success: true,
        message: "Create new room success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async deleteRoom(req, res, next) {
    try {
      const companyId = req.headers["company"] || null;
      const setting = await Setting.findOne({
        company: ObjectId(companyId),
      });
      if (setting && setting.openQueue) {
        return res.status(200).json({
          message: "Must close queue in setting before delete room",
        });
      }

      await Room.removeOne({
        _id: ObjectId(req.body._id),
      });
      const existRooms = await Room.find({
        company: ObjectId(companyId),
        deleted: false,
      }).sort({
        number: 1,
      });

      for (let [index, room] of existRooms.entries()) {
        await Room.updateOne(
          {
            _id: room._id,
          },
          {
            $set: {
              number: index + 1,
            },
          }
        );
      }

      return res.status(200).json({
        success: true,
        message: "Delete room success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
};

module.exports = RoomController;
