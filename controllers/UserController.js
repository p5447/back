const bcrypt = require("bcrypt");
const { ObjectId } = require("mongodb");
const uuid = require("uuid");
const config = require("../configs");
const Company = require("../models/Company");
const User = require("../models/User");

const UserController = {
  async initSuperAdmin() {
    try {
      const existSuperAdmin = await User.findOne({ email: "admin@gmail.com" });
      if (!existSuperAdmin) {
        newPassword = bcrypt.hashSync(
          "admin@admin",
          config.passwordSaltRound,
          (hash, err) => {
            return hash;
          }
        );
        const newSuperAdmin = new User({
          email: "admin@gmail.com",
          password: newPassword,
          name: "Super Admin",
          tel: "0883537410",
          permission: "super_admin",
          company: null,
        });
        await newSuperAdmin.save();
        return console.log("create super admin.");
      }
      return console.log("super admin is exist.");
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async getUsers(req, res, next) {
    try {
      const users =
        req.params.companyId &&
        req.params.companyId !== "null" &&
        req.params.companyId !== "undefined"
          ? await User.find(
              {
                company: ObjectId(req.params.companyId),
                deleted: false,
              },
              { password: 0 }
            ).sort({
              _id: -1,
            })
          : await User.find(
              {
                company: null,
                deleted: false,
              },
              { password: 0 }
            ).sort({
              _id: -1,
            });
      return res.status(200).json({
        success: true,
        message: "Get users success",
        data: users,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async getUserById(req, res, next) {
    try {
      const user = await User.findOne(
        { _id: ObjectId(req.params._id) },
        { password: 0 }
      ).populate("company");
      if (!user) {
        return res.status(200).json({
          message: "User not found.",
        });
      }

      return res.status(200).json({
        success: true,
        message: "Get user success",
        data: user,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async createUser(req, res, next) {
    try {
      const body = {
        ...req.body,
        uuid: uuid.v4(),
      };

      const createdBy = await User.findOne({
        _id: ObjectId(req.headers["userId"]),
      });

      if (
        createdBy &&
        createdBy.permission === "super_admin" &&
        !body.company
      ) {
        body.company = null;
      } else {
        const existCompany = await Company.findOne({
          _id: ObjectId(body.company),
        });
        if (!existCompany) {
          return res.status(200).json({
            message: "Company not found.",
          });
        }
      }

      const existUser = await User.findOne({ email: body.email.trim() });
      if (existUser) {
        return res.status(200).json({
          message: "Email is exist.",
        });
      }

      if (body.password.trim().length < 8) {
        return res.status(200).json({
          message: "Password should be at least 8 character.",
        });
      }

      if (body.re_password.trim() !== body.password.trim()) {
        return res.status(200).json({
          message: "Password and re-password not match.",
        });
      }

      body.password = bcrypt.hashSync(
        body.password,
        config.passwordSaltRound,
        (hash, err) => {
          if (err) {
            console.log(error);
            return res.status(500).json({
              message: "Internal Server Error.",
            });
          }
          return hash;
        }
      );

      const newUser = new User({
        email: body.email,
        password: body.password,
        name: body.name,
        tel: body.tel,
        permission: body.permission,
        company: body.company,
      });

      await newUser.save();

      return res.status(200).json({
        success: true,
        message: "Create user success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async editUser(req, res, next) {
    try {
      const body = {
        ...req.body,
      };

      const existUser = await User.findOne({ email: body.email.trim() });
      if (!existUser) {
        return res.status(200).json({
          message: "User not found",
        });
      }

      await User.updateOne(
        {
          _id: ObjectId(body._id),
        },
        {
          $set: {
            name: body.name,
            tel: body.tel,
            permission: body.permission,
          },
        }
      );

      return res.status(200).json({
        success: true,
        message: "Edit user success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async deleteUser(req, res, next) {
    try {
      const existUser = await User.findOne({
        _id: ObjectId(req.body._id),
      });

      if (!existUser || existUser.deleted) {
        return res.status(200).json({
          message: "User not found.",
        });
      }

      await User.removeOne({
        _id: ObjectId(req.body._id),
      });

      return res.status(200).json({
        success: true,
        message: "Delete user success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async resetPassword(req, res, next) {
    try {
      const body = {
        ...req.body,
      };

      if (body.password.trim().length < 8) {
        return res.status(200).json({
          message: "Password should be at least 8 character.",
        });
      }

      if (body.re_password.trim() !== body.password.trim()) {
        return res.status(200).json({
          message: "Password and re-password not match.",
        });
      }

      body.password = bcrypt.hashSync(
        body.password,
        config.passwordSaltRound,
        (hash, err) => {
          if (err) {
            console.log(error);
            return res.status(500).json({
              message: "Internal Server Error.",
            });
          }
          return hash;
        }
      );

      await User.updateOne(
        {
          _id: ObjectId(body._id),
        },
        {
          $set: {
            password: body.password,
          },
        }
      );

      return res.status(200).json({
        success: true,
        message: "Reset password success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
};

module.exports = UserController;
