const { ObjectId } = require("mongodb");
const Queue = require("../models/Queue");
const QueueType = require("../models/QueueType");
const Company = require("../models/Company");
const User = require("../models/User");
const moment = require("moment");

const DashboardController = {
  async getDashboard(req, res, next) {
    try {
      const companyId = req.headers["company"] || null;
      const type = req.body.type || ""; //today = td, this month = tm, this year = ty, define period = dp
      let typeDisplay = "";

      const queueTypes = await QueueType.find({
        company: ObjectId(companyId),
      });

      let start = "";
      let end = "";
      let year = "";
      let month = "";
      let now = new Date();

      switch (type) {
        case "td":
          typeDisplay = "Today";
          start = new Date(now.getFullYear(), now.getMonth(), now.getDate()); //01-04-2022 0:00:00
          end = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1); //02-04-2022 0:00:00
          break;
        case "tm":
          typeDisplay = "This month";
          year = now.getFullYear();
          month = now.getMonth();
          start = new Date(year, month, 1);
          end = new Date(year, month + 1, 1);
          break;
        case "ty":
          typeDisplay = "This year";
          year = now.getFullYear();
          start = new Date(year, 0, 1); // 0 - 11
          end = new Date(year + 1, 0, 1);
          break;
        case "dp":
          typeDisplay = "Define Period";
          start = new Date(moment(req.body.start).format('YYYY-MM-DD'));
          end = new Date(`${moment(req.body.start).format('YYYY-MM-DD')} 23:59:00`);
          break;
      }

      let response = [];
      for (const queueType of queueTypes) {
        const queueSuccess = await Queue.countDocuments({
          queueType: ObjectId(queueType._id),
          company: ObjectId(companyId),
          waiting: false,
          successBy: {
            $ne: null,
          },
          updatedAt: { $gte: start, $lt: end },
        });

        const queueDestroy = await Queue.countDocuments({
          queueType: ObjectId(queueType._id),
          company: ObjectId(companyId),
          waiting: false,
          successBy: null,
          updatedAt: { $gte: start, $lt: end },
        });

        const queueWaiting = await Queue.countDocuments({
          queueType: ObjectId(queueType._id),
          company: ObjectId(companyId),
          waiting: true,
          updatedAt: { $gte: start, $lt: end },
        });

        response.push({
          queueType: queueType.title,
          type: typeDisplay,
          start: start,
          end: end,
          success: queueSuccess,
          destroy: queueDestroy,
          waiting: queueWaiting,
        });
      }

      const users = await User.find({ company: ObjectId(companyId) });
      let responseByUser = [];
      for (const user of users) {
        const queueSuccess = await Queue.countDocuments({
          successBy: ObjectId(user._id),
          updatedAt: { $gte: start, $lt: end },
        });

        const queueDestroy = await Queue.countDocuments({
          destroyBy: ObjectId(user._id),
          updatedAt: { $gte: start, $lt: end },
        });

        responseByUser.push({
          user: user,
          type: typeDisplay,
          start: start,
          end: end,
          success: queueSuccess,
          destroy: queueDestroy,
        });
      }

      return res.status(200).json({
        success: true,
        message: "get dashboard success",
        data: { dashboard: response, dashboardUser: responseByUser },
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async getDashboardAdmin(req, res, next) {
    try {
      const response = {
        company: 0,
        user: {
          user: 0,
          admin: 0,
          super_admin: 0,
        },
        queue_created: 0,
      };

      response.company = await Company.countDocuments({});
      response.user.user = await User.countDocuments({
        permission: "user",
        deleted: false,
      });
      response.user.admin = await User.countDocuments({
        permission: "admin",
        deleted: false,
      });
      response.user.super_admin = await User.countDocuments({
        permission: "super_admin",
        deleted: false,
      });
      response.queue_created = await Queue.countDocuments({});

      return res.status(200).json({
        success: true,
        message: "get dashboard success",
        data: response,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
};

module.exports = DashboardController;
