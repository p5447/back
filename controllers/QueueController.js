const { ObjectId } = require("mongodb");
const Queue = require("../models/Queue");
const QueueType = require("../models/QueueType");
const Room = require("../models/Room");
const SettingHistory = require("../models/SettingHistory");
const moment = require("moment");

const QueueController = {
  async getQueueById(req, res, next) {
    try {
      const queue = await Queue.findOne({
        _id: ObjectId(req.params.queueId),
      })
        .populate("queueType")
        .populate("company")
        .populate("successBy")
        .lean();
        
      if (!queue) {
        return res.status(200).json({
          message: "Queue not found.",
        });
      }

      const findRoomExistQueue = await Room.findOne({
        company: ObjectId(queue.company._id),
        currentQueue: ObjectId(queue._id),
      });
 

      if (findRoomExistQueue) {
        queue['doingAt'] = findRoomExistQueue;
        console.log('new assign')
      }
      // console.log(queue)

      return res.status(200).json({
        success: true,
        message: "Get queue success",
        data: queue,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async createQueue(req, res, next) {
    try {
      const companyId = req.headers["company"] || null;
      if (!companyId) {
        return res.status(200).json({
          message: "Company is invalid.",
        });
      }

      const queueTypeExist = await QueueType.findOne({
        _id: ObjectId(req.body.queueTypeId),
      });
      if (!queueTypeExist) {
        return res.status(200).json({
          message: "Queue type not found",
        });
      }

      const lastQueue = await Queue.findOne({
        company: ObjectId(companyId),
      }).sort({
        _id: -1,
      });

      const lastQueueForQueueType = await Queue.findOne({
        company: ObjectId(companyId),
        queueType: ObjectId(req.body.queueTypeId),
      }).sort({
        _id: -1,
      });

      const lastOffSetting = await SettingHistory.findOne({
        company: ObjectId(companyId),
        openQueue: false,
      }).sort({
        _id: -1,
      });

      let queueNumber = 1;
      if (lastQueue) {
        if (lastOffSetting) {
          if (
            moment(lastQueue.createdAt).diff(moment(lastOffSetting.createdAt)) >
            0
          ) {
            queueNumber = Number(lastQueue.no) + 1;
          }
        } else {
          queueNumber = Number(lastQueue.no) + 1;
        }
      }

      let queueNumberForQueueType = 1;
      if (lastQueueForQueueType) {
        if (lastOffSetting) {
          if (
            moment(lastQueueForQueueType.createdAt).diff(
              moment(lastOffSetting.createdAt)
            ) > 0
          ) {
            queueNumberForQueueType =
              Number(lastQueueForQueueType.noForQueueType) + 1;
          }
        } else {
          queueNumberForQueueType =
            Number(lastQueueForQueueType.noForQueueType) + 1;
        }
      }

      const queue = new Queue({
        queueType: queueTypeExist._id,
        no: queueNumber,
        noForQueueType: queueNumberForQueueType,
        name: req.body.name.trim(),
        tel: req.body.tel.trim(),
        company: ObjectId(companyId),
      });

      await queue.save();

      return res.status(200).json({
        success: true,
        message: "Create queue success",
        data: queue,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },

  async nextQueue(req, res, next) {
    try {
      const companyId = req.headers["company"] || null;
      const updateBy = req.headers["userId"] || null;
      const { roomId, destroy } = req.body;
      const room = await Room.findOne({
        _id: ObjectId(roomId),
        deleted: false,
      }).populate("currentQueue");

      if (!room) {
        return res.status(200).json({
          message: "Room not found.",
        });
      }

      let rooms = await Room.find({
        company: ObjectId(companyId),
        deleted: false,
      }).populate("currentQueue");

      rooms = rooms.filter((value) => {
        for (const queueType of value.queueTypes) {
          if (room.queueTypes.includes(queueType)) return value;
        }
      });

      if (rooms.length === rooms.filter((_r) => !_r.currentQueue).length) {
        const firstQueue = await Queue.findOne({
          company: ObjectId(companyId),
          queueType: { $in: room.queueTypes.map((value) => ObjectId(value)) },
          waiting: true,
        }).sort({
          no: 1,
        });
        if (!firstQueue) {
          return res.status(200).json({
            message: "No queue for next !",
          });
        }

        await Room.updateOne(
          {
            _id: ObjectId(room._id),
          },
          {
            $set: {
              currentQueue: firstQueue._id,
            },
          }
        );

        return res.status(200).json({
          success: true,
          message: "next queue success",
          data: firstQueue._id,
        });
      } else {
        const roomHasMaxQueue = rooms.reduce((prev, current) =>
          (prev.currentQueue ? prev.currentQueue.no : 0) >
          (current.currentQueue ? current.currentQueue.no : 0)
            ? prev
            : current
        );

        // update current queue
        if (room.currentQueue && room.currentQueue.waiting) {
          await Queue.updateOne(
            {
              _id: ObjectId(room.currentQueue._id),
            },
            {
              $set: {
                waiting: false,
                ...(!destroy
                  ? { successBy: ObjectId(updateBy) }
                  : { destroyBy: ObjectId(updateBy) }),
              },
            }
          );
        }

        const nextQueue = await Queue.findOne({
          company: ObjectId(companyId),
          queueType: { $in: room.queueTypes.map((value) => ObjectId(value)) },
          waiting: true,
          no: {
            $nin: rooms
              .filter(
                (item) =>
                  item.currentQueue !== null && item.currentQueue !== undefined
              )
              .map((item) => item.currentQueue.no),
          },
        }).sort({
          no: 1,
        });

        if (!nextQueue) {
          return res.status(200).json({
            success: true,
            message: "Updated, but no queue for next !",
          });
        }

        // update current queue in room
        await Room.updateOne(
          {
            _id: ObjectId(room._id),
          },
          {
            $set: {
              currentQueue: nextQueue._id,
            },
          }
        );

        return res.status(200).json({
          success: true,
          message: "next queue success",
          data: nextQueue._id,
        });
      }
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },

  async getForwardQueue(req, res, next) {
    try {
      const companyId = req.headers["company"] || null;
      const roomId = req.params.roomId;
      const rooms = await Room.find({
        company: ObjectId(companyId),
      });

      if (roomId === "all") {
        const response = await Queue.find({
          _id: {
            $nin: rooms
              .filter(
                (item) =>
                  item.currentQueue !== null && item.currentQueue !== undefined
              )
              .map((item) => item.currentQueue),
          },
          company: ObjectId(companyId),
          waiting: true,
        })
          .sort({
            no: 1,
          })
          .populate("queueType")
          .limit(5);
        return res.status(200).json({
          success: true,
          message: "Get forward queue success",
          data: response || [],
        });
      } else {
        const room = await Room.findOne({
          _id: ObjectId(roomId),
        });

        if (!room) {
          return res.status(200).json({
            message: "Room not found",
          });
        }

        const response = await Queue.find({
          _id: {
            $nin: rooms
              .filter(
                (item) =>
                  item.currentQueue !== null && item.currentQueue !== undefined
              )
              .map((item) => item.currentQueue),
          },
          company: ObjectId(companyId),
          queueType: { $in: room.queueTypes.map((item) => ObjectId(item)) },
          waiting: true,
        })
          .sort({
            no: 1,
          })
          .populate("queueType")
          .limit(5);
        return res.status(200).json({
          success: true,
          message: "Get forward queue success",
          data: response || [],
        });
      }
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
};

module.exports = QueueController;
