const User = require("../models/User");
const bcrypt = require("bcrypt");
const config = require("../configs");
const jwt = require("jsonwebtoken");

const AuthController = {
  async login(req, res, next) {
    try {
      const email = req.body.email.trim(),
        password = req.body.password.trim();

      let existUser = await User.findOne({ email: email }).populate("company");
      if (!existUser) {
        return res.status(200).json({
          message: "User not found.",
        });
      }

      const validPassword = await bcrypt.compare(password, existUser.password);

      if (!validPassword) {
        return res.status(200).json({
          message: "Invalid user.",
        });
      }

      existUser.password = undefined;

      const token = await jwt.sign(
        {
          ...existUser._doc,
        },
        config.jwtTokenSignature,
        {
          expiresIn: config.tokenLife,
        }
      );

      const refreshToken = await jwt.sign(
        {
          ...existUser._doc,
        },
        config.jwtRefreshTokenSignature,
        {
          expiresIn: config.refreshTokenLife,
        }
      );
      existUser = {
        ...existUser._doc,
        token,
        refreshToken,
      };

      return res.status(200).json({
        success: true,
        message: "Login success",
        data: existUser,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async newToken(req, res, next) {
    try {
      // if refresh token exists
      if (req.body.refreshToken && req.body.email) {
        let existUser = await User.findOne({ email: req.body.email }).populate(
          "company"
        );
        if (!existUser) {
          return res.status(200).json({
            message: "User not found.",
          });
        }
        existUser.password = undefined;
        const token = await jwt.sign(
          { ...existUser._doc },
          config.jwtTokenSignature,
          {
            expiresIn: config.tokenLife,
          }
        );

        return res.status(200).json({
          success: true,
          message: "Login success",
          data: {
            token,
          },
        });
      } else {
        res.status(400).json({
          message: "Invalid request.",
        });
      }
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
};

module.exports = AuthController;
