const Setting = require("../models/Setting");
const Queue = require("../models/Queue");
const Room = require("../models/Room");
const { ObjectId } = require("mongodb");
const SettingHistory = require("../models/SettingHistory");

const SettingController = {
  async initSetting(companyId) {
    try {
      const setting = await Setting.findOne({ company: ObjectId(companyId) });
      if (!setting) {
        const newSetting = new Setting({
          company: ObjectId(companyId),
        });
        await newSetting.save();
        return true;
      }
    } catch (error) {
      return false;
    }
  },
  async openQueue(req, res, next) {
    try {
      const companyId = req.headers["company"] || null;
      await Setting.updateOne(
        { company: ObjectId(companyId) },
        {
          $set: {
            openQueue: true,
          },
        }
      );

      const newHistory = new SettingHistory({
        openQueue: true,
        company: ObjectId(companyId),
      });

      await newHistory.save();

      return res.status(200).json({
        success: true,
        message: "open queue success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async closeQueue(req, res, next) {
    try {
      const companyId = req.headers["company"] || null;
      await Setting.updateOne(
        { company: ObjectId(companyId) },
        {
          $set: {
            openQueue: false,
          },
        }
      );

      await Room.updateMany(
        {
          company: ObjectId(companyId),
          currentQueue: {
            $ne: null,
          },
          deleted: false,
        },
        {
          $set: {
            currentQueue: null,
          },
        }
      );

      await Queue.updateMany(
        {
          company: ObjectId(companyId),
          waiting: true,
        },
        {
          $set: {
            waiting: false,
          },
        }
      );

      const newHistory = new SettingHistory({
        openQueue: false,
        company: ObjectId(companyId),
      });

      await newHistory.save();

      return res.status(200).json({
        success: true,
        message: "close queue success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
  async isOpenQueue(req, res, next) {
    try {
      const companyId = req.headers["company"] || null;
      const setting = await Setting.findOne({ company: ObjectId(companyId) });
      if (!setting) {
        return res.status(200).json({
          success: true,
          message: "get queue open",
          data: false,
        });
      }

      return res.status(200).json({
        success: true,
        message: "get queue open",
        data: setting.openQueue,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Server Error.",
      });
    }
  },
};

module.exports = SettingController;
